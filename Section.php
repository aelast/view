<?php

namespace Aelast\View;

class Section
{

    const SECTION_START = 'start';
    const SECTION_READY = 'ready';
    const SECTION_APPEND = 'append';
    const SECTION_PREPEND = 'prepend';
    const SECTION_REPLACE = 'replace';

    private $sections = [];
    private $name = 'default';

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function open($type = self::SECTION_APPEND)
    {
        $this->sections[] = ['type' => $type, 'status' => self::SECTION_START, 'content' => ''];
        ob_start();
    }

    public function close($index = null)
    {
        if (is_null($index)) {
            $index = $this->getLastOpenedSectionIndex();
        }
        if ($index === false) {
            return;
        }
        $this->sections[$index]['content'] = ob_get_contents();
        $this->sections[$index]['status'] = self::SECTION_READY;
        ob_end_clean();
    }

    private function getLastOpenedSectionIndex()
    {
        for ($i = count($this->sections) - 1; $i >= 0; $i--) {
            if ($this->sections[$i]['status'] === self::SECTION_START) {
                return $i;
            }
        }
        return false;
    }

    public function render()
    {
        $content = '';
        $prev_section_type = self::SECTION_APPEND;
        for ($i = 0; $i < count($this->sections); $i++) {
            if ($prev_section_type === self::SECTION_APPEND) {
                $content = $this->sections[$i]['content'] . $content;
            } elseif ($prev_section_type === self::SECTION_PREPEND) {
                $content .= $this->sections[$i]['content'];
            }
            if ($this->sections[$i]['type'] === self::SECTION_REPLACE) {
                break;
            }
            $prev_section_type = $this->sections[$i]['type'];
        }
        $this->clear();
        echo $content;
    }

    private function clear()
    {
        $this->sections = [];
    }

}
