<?php

namespace Aelast\View;

use Aelast\View\Section as Section;

trait SectionTrait
{

    protected $sections = [];
    protected $sections_stack = [];

    public function openSection($name, $type = Section::SECTION_APPEND)
    {
        if (!isset($this->sections[$name])) {
            $this->sections[$name] = new Section($name);
        }
        $this->sections[$name]->open($type);
        $this->sections_stack[] = $this->sections[$name];
    }

    public function closeSection($render = false)
    {
        if (count($this->sections_stack) == 0) {
            return;
        }
        end($this->sections_stack)->close();
        if ($render === true) {
            end($this->sections_stack)->render();
        }
    }

    public function removeSection($name)
    {
        $this->openSection($name, Section::SECTION_REPLACE);
        $this->closeSection();
    }

    public function declareSection($name)
    {
        $this->openSection($name);
        $this->closeSection(true);
    }

}
