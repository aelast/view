<?php

namespace Aelast\View;

class SeparateWidget extends Widget
{

    public function __get($name)
    {
        if (!isset($this->vars[$name])) {
            return null;
        }
        return $this->vars[$name];
    }

    public function __call($name, $arguments)
    {
        return call_user_func([$this->parent, $name], $arguments);
    }

    public function __isset($name)
    {
        return isset($this->vars[$name]);
    }

    public function __set($name, $value)
    {
        unset($name, $value);
        return false;
    }

}
