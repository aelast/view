<?php

namespace Aelast\View;

class Widget implements WidgetInterface
{

    use SectionTrait;

    protected $vars;
    protected $template;
    protected $content = '';
    protected $uid;
    protected $parent;
    protected $parents;

    public function __construct($parent, Array $vars = [])
    {
        $this->parent = $parent;
        $this->vars = $vars;
        $this->uid = md5(get_class($this) . '***' . json_encode($vars));
        $this->parents = is_array($this->parent->parents) ? $this->parent->parents : [];
        if (in_array($this->uid, $this->parents)) {
            throw new \Exception('Recursive inclusion');
        }
        $this->parents[] = $this->uid;
        $this->sections = $this->parent->sections;
        $this->sections_stack = $this->parent->sections_stack;
    }

    public function begin()
    {
        ob_start();
    }

    public function finish()
    {
        $this->content = ob_get_contents();
        ob_end_clean();
    }

    public function render()
    {
        $this->begin();
        if (!empty($this->template)) {
            $this->fetchTemplate();
        }
        $this->finish();
        echo $this->content;
    }

    private function fetchTemplate()
    {
        $this->loadTemplate($this->template);
    }

    private function loadTemplate($filename)
    {
        foreach($this->getPaths($this->uid) as $path) {
            if (is_file($path . $filename)) {
                include ($path . $filename);
                return;
            }
        }
        throw new \Exception('Template "' . $filename . '" not found');
    }

    protected function getUid()
    {
        return $this->uid;
    }
    
    public function getContent()
    {
        return $this->content;
    }
    
    public function get($name, $default = null)
    {
        if (isset($this->vars[$name])) {
            return $this->vars[$name];
        }
        return $this->parent->get($name, $default);
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->parent, $name], $arguments);
    }

    public function __isset($name)
    {
        return isset($this->parent->$name);
    }

}
