<?php

namespace Aelast\View;

interface ViewInterface
{

    public function render();

    public function getContent();
}
