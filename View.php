<?php

namespace Aelast\View;

use Aelast\Tools\Strings as Strings;

class View implements ViewInterface
{

    use SectionTrait;

    protected $paths = [];
    protected $layout;
    protected $template;
    protected $vars = [];
    protected $content = '';
    protected $rendered = false;
    protected $charset = 'UTF-8';

    public function __construct($vars = [])
    {
        $this->assignVars($vars);
    }

    public function render()
    {
        if (empty($this->template)) {
            throw new \Exception('Template not declared');
        } else {
            $this->fetchTemplate();
        }
        if (!empty($this->layout)) {
            $this->fetchLayout();
        }
        $this->rendered = true;
    }

    public function show()
    {
        if (!$this->rendered) {
            $this->render();
        }
        echo $this->getContent();
    }

    public function assignVars($vars)
    {
        if (!is_array($vars)) {
            return;
        }
        foreach ($vars as $name => $value) {
            $this->set($name, $value);
        }
    }

    private function fetchLayout()
    {
        ob_start();
        $this->loadTemplate($this->layout);
        $this->content = ob_get_contents();
        ob_end_clean();
    }

    private function fetchTemplate()
    {
        ob_start();
        $this->loadTemplate($this->template);
        $this->content = ob_get_contents();
        ob_end_clean();
    }

    private function loadTemplate($filename)
    {
        foreach ($this->getPaths() as $path) {
            if (is_file($path . $filename)) {
                include ($path . $filename);
                return;
            }
        }
        throw new \Exception('Template "' . $filename . '" not found');
    }

    public function isRendered()
    {
        return $this->rendered;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    public function getCharset()
    {
        return $this->charset;
    }

    public function addPaths($paths, $namespace = '__main__')
    {
        if (!is_array($paths)) {
            $paths = [$paths];
        }
        if (!isset($this->paths[$namespace])) {
            $this->paths[$namespace] = [];
        }
        foreach ($paths as $path) {
            $prepared_path = rtrim(preg_replace('#/{2,}#', '/', str_replace('\\', '/', (string) $path)), '/');
            if (!is_dir($prepared_path)) {
                continue;
            }
            $this->paths[$namespace][] = $prepared_path . '/';
        }
    }

    public function getPaths($namespace = '__main__')
    {
        return isset($this->paths[$namespace]) ? $this->paths[$namespace] : [];
    }

    public function escape($string, $type = 'html', $double = true)
    {
        return Strings::escape($string, $type, $this->charset, $double);
    }

    public function set($name, $value)
    {
        $this->vars[$name] = $value;
        return $this;
    }

    public function __set($name, $value)
    {
        return $this->set($name, $value);
    }

    public function assign($name, $value)
    {
        return $this->set($name, $value);
    }

    public function get($name, $default = null)
    {
        if (!isset($this->vars[$name])) {
            return $default;
        }
        return $this->vars[$name];
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __call($name, $arguments)
    {
        if (strpos($name, 'get') !== 0 && strpos($name, 'set') !== 0) {
            throw new \Exception('Method "' . $name . '" not exists');
        }
        $segments = preg_split('/(?=[A-Z])/', $name);
        array_shift($segments);
        $var_name = strtolower(implode('_', $segments));
        if (strpos($name, 'get') === 0) {
            return $this->get($var_name);
        } elseif (strpos($name, 'set') === 0) {
            return $this->set($var_name, isset($arguments[0]) ? $arguments[0] : null);
        }
    }

    public function __isset($name)
    {
        return isset($this->vars[$name]);
    }

}
