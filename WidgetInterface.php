<?php

namespace Aelast\View;

interface WidgetInterface extends ViewInterface
{

    public function begin();

    public function finish();
}
